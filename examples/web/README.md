# YanZhi (Face Value) 

- Recognize faces from an uploaded picture
- Compare each face with pre-defined list of faces as references (Top 10 most beautiful women as default )
- Return the winer face which has the smallest distance to the references. 


Built using [dlib](http://dlib.net/)'s state-of-the-art face recognition
built with deep learning. The model has an accuracy of 99.38% on the
[Labeled Faces in the Wild](http://vis-www.cs.umass.edu/lfw/) benchmark.

This also provides a simple `face_recognition` command line tool that lets
you do face recognition on a folder of images from the command line!


[![PyPI](https://img.shields.io/pypi/v/face_recognition.svg)](https://pypi.python.org/pypi/face_recognition)
[![Build Status](https://github.com/ageitgey/face_recognition/workflows/CI/badge.svg?branch=master&event=push)](https://github.com/ageitgey/face_recognition/actions?query=workflow%3ACI)
[![Documentation Status](https://readthedocs.org/projects/face-recognition/badge/?version=latest)](http://face-recognition.readthedocs.io/en/latest/?badge=latest)

## Steps

#### Clone 

Find all the faces that appear in a picture:

![](https://cloud.githubusercontent.com/assets/896692/23625227/42c65360-025d-11e7-94ea-b12f28cb34b4.png)

```
git clone https://gitlab.com/lishengtao/yanzhi
```

#### Build the docker image

Get the locations and outlines of each person's eyes, nose, mouth and chin.

![](https://cloud.githubusercontent.com/assets/896692/23625282/7f2d79dc-025d-11e7-8728-d8924596f8fa.png)

```python
docker build -t yanzhi:v1 .
```

#### Run docker container 

```
docker run -dit --name yz_1 -p 80:5001 yanzhi:v1
```
#### Adding gitlab runner to Dev Ubuntu VM

```
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

apt-get install gitlab-runner




```

#### Adding gitlab runner to AWS Prod VM

```
[ec2-user@ip-172-31-80-30 ~]$ sudo curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash

[ec2-user@ip-172-31-80-30 ~]$ sudo yum install gitlab-runner.x86_64

# get token from https://gitlab.com/lishengtao/yanzhi/-/settings/ci_cd
[ec2-user@ip-172-31-80-30 ~]$ sudo gitlab-runner register

[ec2-user@ip-172-31-80-30 ~]$ sudo vim /etc/sudoers
Add line of - gitlab-runner ALL=(ALL) NOPASSWD: ALL

[ec2-user@ip-172-31-80-30 ~]$ sudo usermod -aG docker gitlab-runner


[ec2-user@ip-172-31-80-30 ~]$ sudo gitlab-runner restart

[ec2-user@ip-172-31-80-30 ~]$ sudo gitlab-runner status


```
## Thanks
