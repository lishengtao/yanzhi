# Created by Shengtao Li Based on the following:
#   - find_faces_in_pictures.py
#   - face_distance.py
# 
# updated: 2020-04-21

from PIL import Image
import face_recognition

#from tempfile import NamedTemporaryFile
#from shutil import copyfileobj
#from os import remove
from flask import Flask, jsonify, request, redirect, send_file, render_template
import io
import numpy
import base64

# You can change this to any folder on your system
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

app = Flask(__name__)
app.config.update(
    TESTING=True,
    EXPLAIN_TEMPLATE_LOADING=True
)


#output = io.BytesIO()

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/', methods=['GET', 'POST'])
def upload_image():
    # Web_DIR = "/home/shengtao/git/face_recognition/examples/web/"
    # Check if a valid image file was uploaded
    if request.method == 'POST':
        if 'file' not in request.files:
            return redirect(request.url)

        file = request.files['file']

        if file.filename == '':
            return redirect(request.url)

        if file and allowed_file(file.filename):
            # The image file seems valid! Detect faces and return the result.
            return find_and_rank(file)

    # If no valid image file was uploaded, show the file upload form:
    return render_template('home.html', message="Hello")

#@app.route('/return_file/')
#def return_file():
#    global output
#    return send_file(output, mimetype='image/png', as_attachment=False)

def find_and_rank(file_stream):
    #global output

    #DIR = "/home/shengtao/git/yanzhi/examples/web"
    DIR = "/root/yanzhi"
    # Load some images to compare against
    list_ref_image = []
    for i in range(0,15):
        list_ref_image.append(face_recognition.load_image_file(DIR+"/ref_pics/"+ str(i+1) + ".jpg"))

    # sorry
    sorry_image = face_recognition.load_image_file(DIR+"/sorry.jpg")

    # Get the face encodings for the known images
    list_ref_encoding = []
    for i in range(0,15):
        list_ref_encoding.append(face_recognition.face_encodings(list_ref_image[i])[0])
    
    # Load the group picture jpg file into a numpy array
    image = face_recognition.load_image_file(file_stream)

    # Find all the faces in the image using the default HOG-based model.
    # This method is fairly accurate, but not as accurate as the CNN model and not GPU accelerated.
    # See also: find_faces_in_picture_cnn.py
    face_locations = face_recognition.face_locations(image)

    print("I found {} face(s) in this photograph.".format(len(face_locations)))

    best_dist = 1.00
    n = 0
    best_index = 0
    best_image = Image.fromarray(sorry_image)
    ref_image = Image.fromarray(sorry_image)

    for face_location in face_locations:

        # Print the location of each face in this image
        top, right, bottom, left = face_location
        ## print("A face is located at pixel location Top: {}, Left: {}, Bottom: {}, Right: {}".format(top, left, bottom, right))

        # You can access the actual face itself like this:
        face_image = image[top:bottom, left:right]
        pil_image = Image.fromarray(face_image)
        #pil_image.show()

        image_to_test_encoding = face_recognition.face_encodings(image,{face_location},1)

        #image_to_test_encoding = face_recognition.face_encodings(face_image)[0]
        # See how far apart the test image is from the known faces
        face_distances = face_recognition.face_distance(list_ref_encoding, image_to_test_encoding[0])
        
        n = n + 1
        if min(face_distances) < best_dist:
            best_dist = min(face_distances)
            best_index = n
            best_ref = numpy.where(face_distances == min(face_distances))[0][0]
            best_image = pil_image
            ref_image = Image.fromarray(list_ref_image[best_ref])

    #print("The best matching face #{} value is {}".format(best_index, best_image))
    #print("The ref of best matching face #{} is {}".format(best_index, best_ref))

    output1 = io.BytesIO()
    best_image.convert('RGBA').save(output1, format='PNG')
    #output.seek(0, 0)
    img = base64.b64encode(output1.getvalue())

    output2 = io.BytesIO()
    ref_image.convert('RGBA').save(output2, format='PNG')
    #output.seek(0, 0)
    ref_img = base64.b64encode(output2.getvalue())


    #return send_file(output, mimetype='image/png', as_attachment=False)
    return render_template('result.html',  img=img.decode('ascii'), dist = f'{(1 - best_dist):5f}', ref_img=ref_img.decode('ascii')) 
    # return render_template('result.html')    


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001, debug=True)